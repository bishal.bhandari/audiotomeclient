import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import "./Navbar.css";
import logo from "../assets/New Project.png";
import DefaultUser from "../assets/defaultuser.png";

const NavMenu = [
  { name: "Home", link: "/" },
  { name: "Audio Book", link: "/audio_books" },
  { name: "Notes", link: "/notes" },
  { name: "About Us", link: "/aboutus" },
  { name: "FAQ", link: "/faq" }
];

const SubMenu = [
  { name: "Profile", link: "/profile" },
  { name: "Listen List", link: "#" },
  { name: "Change password", link: "/change-password" }
];

export default function NavBar() {

  let userData = JSON.parse(localStorage.getItem("user"))

  if (localStorage.getItem("token") === null) {
    window.location.assign("/login");
  }
  if (userData.role === "ADMIN") {
    window.location.assign("/admin/dashboard");
  }

  const Logout = () => {
    localStorage.clear();
  };

  useEffect(
    () => {
      if (userData === null) {
        window.location.assign("/login");
      }
    },
    [localStorage.getItem("token") !== null]
  );

  const DefUser = () => {
    return (
      <img
        src={DefaultUser}
        className="rounded-circle"
        height="35"
        alt="PIC"
        loading="lazy"
      />
    );
  };

  const UserProfile = profile => {
    return (
      <img
        src={profile}
        className="rounded-circle"
        height="30"
        alt="PIC"
        loading="lazy"
      />
    );
  };

  return (
    <nav className="row customrowcss sticky-top">
      <div className="col-sm-5 customcolcss d-flex justify-content-between align-items-center">
        <div className="mx-5">
          <Link className="navbar-brand mt-2 mt-lg-0" to={"/"}>
            <img src={logo} height="30" alt="AutoTome" loading="lazy" />
          </Link>
        </div>
      </div>
      <div className="col-sm-7">
        <nav className="navbar navbar-expand-lg text-light d-flex align-items-center">
          <div className="container mx-5 p-2">
            <button
              className="navbar-toggler"
              type="button"
              data-mdb-toggle="collapse"
              data-mdb-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <i className="fas fa-bars" />
            </button>

            <div
              className="collapse navbar-collapse"
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav me-auto mb-3 mb-lg-0">
                {NavMenu.map((items, index) => {
                  return (
                    <li className="nav-item" key={index}>
                      <Link to={items.link} className="nav-link text-light">
                        {items.name}
                      </Link>
                    </li>
                  );
                })}
              </ul>
              <div className="d-flex align-items-center">
                <div className="dropdown">
                  <Link
                    className="dropdown-toggle d-flex align-items-center hidden-arrow"
                    to={"#"}
                    id="navbarDropdownMenuAvatar"
                    role="button"
                    data-mdb-toggle="dropdown"
                    aria-expanded="false"
                  >
                    {userData.profileImage ? <UserProfile profile={userData.profileImage} /> : <DefUser />}
                  </Link>
                  <ul
                    className="dropdown-menu dropdown-menu-end"
                    aria-labelledby="navbarDropdownMenuAvatar"
                  >
                    {SubMenu.map((items, index) => {
                      return (
                        <li key={index}>
                          <Link
                            className="dropdown-item text-light"
                            to={items.link}
                          >
                            {items.name}
                          </Link>
                        </li>
                      );
                    })}
                    <li>
                      <Link
                        className="dropdown-item text-light"
                        to={"/login"}
                        onClick={Logout}
                      >
                        Logout
                      </Link>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </nav>
      </div>
    </nav>
  );
}
