import React, { useState, useEffect } from "react";
import "../AudioBook.css";
import { GetAudioBookById } from "../../../environment/UserService";

export default function AudioBookDetail(prop) {

  const [responseData, setResponseData] = useState({});

  const GetAudioBookData = async () => {
    await GetAudioBookById(prop.id).then((resp) => {
      setResponseData(resp.data);
    });
  };

  useEffect(() => {
    GetAudioBookData();
  },[]);
  return (
    <main>
      <div className="row">
        <div className="col-sm-4">
          <div>
            <img
              src="https://mdbcdn.b-cdn.net/img/new/standard/nature/184.webp"
              alt="lets see"
              className="DetailImage"
            />
          </div>
        </div>
        <div className="col-sm-8">
          <h4>{responseData.abookName}</h4>
          <h4>{responseData.abookAuthor}</h4>
          <h4>{responseData.abookName}</h4>
        </div>
      </div>
    </main>
  );
}
