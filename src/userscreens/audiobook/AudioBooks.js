import React, { useEffect, useRef, useState } from "react";
import NavBar from "../../theme/Navbar";
import "./AudioBook.css";
import song from "../../assets/y2mate.com - Its you  Ali Gatie Lyrics.mp3";
import {
  GetAudioBook,
  GetAudioBookByName,
} from "../../environment/UserService";
import AudioBookDetail from "./audioBookDetail/AudioBookDetail";

export default function AudioBooks() {
  const [searchedAudio, setSearchedAudio] = useState("");
  const [isPlaying, setIsPlaying] = useState(false);
  const [currentSong, setCurrentSong] = useState("");

  const [duration, setDuration] = useState(0);

  const [playerData, setPlayerData] = useState("");

  const [audioBook, setAudioBook] = useState([]);

  const audioEle = useRef();

  const TooglePlayPause = (items) => {
    const prevSong = isPlaying;
    setCurrentSong(items);
    setIsPlaying(!prevSong);
    if (prevSong) {
      audioEle.current.play();
    } else {
      audioEle.current.pause();
    }
    setDuration(audioEle.current.duration);
  };

  const Searched = (event) => {
    setSearchedAudio(event.target.value);
  };

  const getAudioBook = async () => {
    await GetAudioBook().then((res) => {
      setAudioBook(res.response);
    });
  };

  const getAudioBookByName = async () => {
    await GetAudioBookByName(searchedAudio).then((res) => {
      setAudioBook(res.response);
    });
  };

  useEffect(() => {
      getAudioBook();
  }, []);

  return (
    <>
      <NavBar />
      <div className="container mt-5 mb-3">
        <div className="d-flex justify-content-end">
          <div className="input-group w-50 mb-3">
            <input
              type="search"
              className="form-control"
              placeholder="Search audio book"
              value={searchedAudio}
              onChange={Searched}
              aria-label="Example text with button addon"
              aria-describedby="button-addon1"
            />
          </div>
        </div>
        <div className="d-flex flex-row justify-content-center flex-wrap">
          {audioBook
            .filter((item) => {
              return searchedAudio.toLowerCase() === ""
                ? item
                : item.abookName.toLowerCase().includes(searchedAudio);
            })
            .map((items, index) => {
              return (
                <div
                  className="card me-3"
                  style={{ width: "17rem" }}
                  key={index}
                  loading="lazy"
                >
                  <img
                    src="https://mdbcdn.b-cdn.net/img/new/standard/nature/184.webp"
                    className="card-img-top"
                    alt="Fissure in Sandstone"
                  />
                  <div className="card-body">
                    <h5 className="card-title">
                      {items.abookName}: By {items.author}
                    </h5>
                  </div>
                  <div className="card-footer d-flex justify-content-around">
                    <button
                      className="btn"
                      onClick={() => {
                        TooglePlayPause(song);
                        setPlayerData(items.title);
                      }}
                    >
                      <i className="fa-solid fa-play"></i>
                    </button>
                    <button
                      className="btn"
                      data-mdb-toggle="modal"
                      data-mdb-target="#exampleModal"
                    >
                      <i className="fa-solid fa-info"></i>
                    </button>
                    <div
                      className="modal fade"
                      id="exampleModal"
                      tabindex="-1"
                      aria-labelledby="exampleModalLabel"
                      aria-hidden="true"
                    >
                      <div className="modal-dialog modal-fullscreen">
                        <div className="modal-content">
                          <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">
                              Details
                            </h5>
                            <button
                              type="button"
                              className="btn-close"
                              data-mdb-dismiss="modal"
                              aria-label="Close"
                            ></button>
                          </div>
                          <div className="modal-body">
                            <AudioBookDetail id={items.aid} />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
        </div>
      </div>
      <footer
        className={
          playerData !== ""
            ? `sticky-bottom position-absolute bottom-0 w-100`
            : "d-none"
        }
      >
        <div className="container">
          <h5 className="text-center text-light mb-3">{currentSong}</h5>
          <audio
            className={playerData !== "" ? `sticky-bottom w-100` : "d-none"}
            controls
            autoPlay
            ref={audioEle}
            src={currentSong}
            type="audio/mp3"
          />
        </div>
      </footer>
    </>
  );
}
