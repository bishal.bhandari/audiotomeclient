import React from "react";

export default function NoteDetails() {
  return (
    <main className="container-fluid">
      <div className="row">
        <div className="col-sm-11">
          <div className="mb-2">
            <h4>Title</h4>
            <p className="text-muted">Hello world</p>
          </div>
          <div>
            <h4>Descriptions</h4>
            <p className="text-muted">lorem</p>
          </div>
        </div>
      </div>
    </main>
  );
}
