import React from "react";

export default function NoteAdd() {
  return (
    <div>
      <div className="form-group mb-2">
        <label>Title</label>
        <input type="text" className="form-control form-control-lg" />
      </div>
      <div className="form-group mb-2">
        <label>Description</label>
        <textarea className="form-control" id="form4Example3" rows="4"></textarea>
      </div>
    </div>
  );
}
