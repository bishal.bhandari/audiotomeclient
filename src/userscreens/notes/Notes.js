import React from "react";
import NavBar from "../../theme/Navbar";
import NoteDetails from "./note_details/NoteDetails";
import NoteEdit from "./note_edit/NoteEdit";
import NoteAdd from "./note_add/NoteAdd";

export default function Notes() {
  const DeleteNote = () => {
    // if(confirm("Are you sure you want to delete this Note?") === true){
    // }
  };

  return (
    <>
      <NavBar />
      <main className="container mt-4">
        <div className="d-flex justify-content-end">
          <button
            type="btn"
            className="btn mb-4"
            data-mdb-toggle="modal"
            data-mdb-target="#exampleModal"
          >
            Add Notes
          </button>
          <div
            className="modal fade"
            id="exampleModal"
            tabIndex="-1"
            aria-labelledby="exampleModalLabel"
            aria-hidden="true"
          >
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-body">
                  <NoteAdd />
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    data-mdb-dismiss="modal"
                  >
                    Cancel
                  </button>
                  <button
                    type="button"
                    className="btn btn-primary"
                    data-mdb-dismiss="modal"
                  >
                    Save changes
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="card">
          <div className="card-body">
            <div className="row">
              <div className="col-sm-9">
                <h3>helloworld</h3>
                <p className="text-muted">
                  lorem ipsum dolor sit amet, consectetur adip
                </p>
              </div>
              <div className="col-sm-2">
                <h6>Date: 2059/345/24</h6>
              </div>
              <div className="col-sm-1 d-flex flex-column">
                <div>
                  <button
                    type="btn"
                    className="btn mb-2 btn-sm"
                    data-mdb-toggle="modal"
                    data-mdb-target="#exampleModalEye"
                  >
                    <i className="fa-solid fa-eye text-primary"></i>
                  </button>
                  <div
                    className="modal fade"
                    id="exampleModalEye"
                    tabIndex="-1"
                    aria-labelledby="exampleModalLabel"
                    aria-hidden="true"
                  >
                    <div className="modal-dialog">
                      <div className="modal-content">
                        <div className="modal-header">
                          <button
                            type="btn"
                            className="btn-close"
                            data-mdb-dismiss="modal"
                            aria-label="Close"
                          ></button>
                        </div>
                        <div className="modal-body">
                          <NoteDetails />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="mb-2">
                  <button
                    type="btn"
                    className="btn btn-sm"
                    data-mdb-toggle="modal"
                    data-mdb-target="#exampleModalEdit"
                  >
                    <i className="fa-solid fa-edit text-info"></i>
                  </button>
                  <div
                    className="modal fade "
                    id="exampleModalEdit"
                    tabIndex="-1"
                    aria-labelledby="exampleModalLabel"
                    aria-hidden="true"
                  >
                    <div className="modal-dialog">
                      <div className="modal-content">
                        <div className="modal-body">
                          <NoteEdit />
                        </div>
                        <div className="modal-footer">
                          <button
                            type="button"
                            className="btn btn-secondary"
                            data-mdb-dismiss="modal"
                          >
                            Cancel
                          </button>
                          <button type="button" className="btn btn-info">
                            Save Changes
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div>
                  <button
                    type="btn"
                    className="btn btn-sm"
                    data-mdb-toggle="modal"
                    data-mdb-target="#exampleModalTrash"
                  >
                    <i className="fa-solid fa-trash text-danger"></i>
                  </button>
                  <div
                    className="modal fade"
                    id="exampleModalTrash"
                    tabIndex="-1"
                    aria-labelledby="exampleModalLabel"
                    aria-hidden="true"
                  >
                    <div className="modal-dialog">
                      <div className="modal-content">
                        <div className="modal-body p-4">
                          <h5 className="text-warning">
                            Are you sure you want to delete this?
                          </h5>
                        </div>
                        <div className="modal-footer">
                          <button
                            type="button"
                            className="btn btn-secondary"
                            data-mdb-dismiss="modal"
                          >
                            Cancel
                          </button>
                          <button type="button" className="btn btn-danger">
                            Confirm
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </>
  );
}
