import React from 'react'
import NavBar from '../../theme/Navbar'
import './Home.css'
import { Link } from 'react-router-dom'

const NOTES = [
  { title: 'Demo', desc: 'Hello World.' },
  { title: 'Demo', desc: 'Hello World.' },
  { title: 'Demo', desc: 'Hello World.' },
  { title: 'Demo', desc: 'Hello World.' },
  { title: 'Demo', desc: 'Hello World.' },
]

const CONTINUELISTENING = [
  { title: 'Demo', author: 'Demo', desc: 'Hello World.' },
  { title: 'Demo', author: 'Demo', desc: 'Hello World.' },
  { title: 'Demo', author: 'Demo', desc: 'Hello World.' },
  { title: 'Demo', author: 'Demo', desc: 'Hello World.' },
  { title: 'Demo', author: 'Demo', desc: 'Hello World.' },
  { title: 'Demo', author: 'Demo', desc: 'Hello World.' },
]

export default function Home() {
  return (
    <>
      <NavBar />
      <main className="container mt-5 mb-5">
        <div className="mb-4">
          <div className="d-flex justify-content-between">
            <p className="fs-4 fw-bold">Notes</p>
            <Link
              to="/notes"
              className="fs-6 fw-bold"
              style={{ color: '#BD00FF' }}
            >
              Show more <i className="fa-solid fa-chevron-right"></i>
            </Link>
          </div>
          <div className="d-flex justify-content-center flex-row flex-wrap">
            {NOTES.map((items, index) => {
              return (
                <div className="card customCard" style={{ width: '17rem' }} key={index}>
                  <div className="card-body">
                    <h5 className="card-title">
                      {items.title}
                    </h5>
                    <p className="card-text">{items.desc}</p>
                  </div>
                </div>
              )
            })}
          </div>
        </div>
        {/* <hr style={{ color: '#BD00FF' }} /> */}
        <div className="mb-4">
          <div className="d-flex justify-content-between">
            <p className="fs-4 fw-bold">Continue Listening</p>
            <Link to="#" className="fs-6 fw-bold" style={{ color: '#BD00FF' }}>
              Show more <i className="fa-solid fa-chevron-right"></i>
            </Link>
          </div>
          <div className='d-flex justify-content-center flex-row flex-wrap'>
            {CONTINUELISTENING.map((items, index) => {
              return (
                <div
                  key={index}
                  style={{ display: 'flex', marginRight: '20px' }}
                >
                  <div className="card" style={{ width: '17rem' }}>
                    <img
                      src="https://mdbcdn.b-cdn.net/img/new/standard/nature/184.webp"
                      className="card-img-top"
                      alt="Fissure in Sandstone"
                    />
                    <div className="card-body">
                      <h5 className="card-title">{items.title}</h5>
                      <p className="card-text">{items.desc}</p>
                    </div>
                  </div>
                </div>
              )
            })}
          </div>
        </div>
      </main>
    </>
  )
}
