import axios from "axios";

// const apiEndPoint = "https://audiotomeserver-production.up.railway.app/api/v1";
const apiEndPoint = "http://localhost:8080/api/v1";

export const GetNotesByUserId = id => {
  return new Promise((resolve, reject) => {
    axios
      .get(`${apiEndPoint}/getNotesByUserId?user_id=${id}`, {
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Access-Origin": "*"
        }
      })
      .then(resp => {
        resolve(resp);
      })
      .catch(err => reject(err));
  });
};
export const GetNotesById = id => {
  return new Promise((resolve, reject) => {
    axios
      .get(`${apiEndPoint}/getNotesById?id=${id}`, {
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Access-Origin": "*"
        }
      })
      .then(resp => {
        resolve(resp);
      })
      .catch(err => reject(err));
  });
};

export const SaveNotes = data => {
  return new Promise((resolve, reject) => {
    axios
      .post(`${apiEndPoint}/notes`, data, {
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*"
        }
      })
      .then(resp => {
        resolve(resp);
      })
      .catch(err => reject(err));
  });
};

export function UpdateNoteById(id, data) {
  return new Promise(function(reject) {
    axios
      .put(`${apiEndPoint}/updateNotesById?id=${id}`, data, {
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*"
        }
      })
      .catch(err => {
        reject(err);
      });
  });
}

export function DeleteNoteById(id) {
  axios.delete(`${apiEndPoint}/delNotesByid?aid=${id}`);
}
