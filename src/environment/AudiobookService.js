import axios from "axios";

const apiEndPoint = "http://localhost:8080/api/v1";

// export const fileUploadUrl = (data) => {
//     return new Promise((resolve, reject)=>{
//         axios.post(apiendpoint, data, )
//     })
// }

export const getAudiobookByListen = () => {
  return new Promise((resolve, reject) => {
    axios
      .post(
        `${apiEndPoint}/audiobook/getAudioBookBasedonListen`,
        {},
        {
          headers: {
            common: {
              "Access-Control-Allow-Origin": "*",
              "Access-Control-Allow-Headers": "Authentication",
              "Content-Type": "application/json",
              Authorization: `Bearer ${localStorage.getItem("token")}`,
              "Referrer-Policy": "no-referrer",
            },
          },
        }
      )
      .then((res) => resolve(res))
      .catch((err) => reject(err));
  });
};

export function GetAudiobook() {
  return new Promise(function (resolve, reject) {
    axios
      .get(`${apiEndPoint}/audiobook`, {
        headers: {
          common: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "Authentication",
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`,
            "Referrer-Policy": "no-referrer",
          },
        },
      })
      .then((response) => {
        resolve(response);
      })
      .catch((err) => {
        reject(err);
      });
  });
}
export function GetAudioBookByStatus(status) {
  return new Promise((resolve, reject) => {
    axios
      .get(
        `${apiEndPoint}/audiobook/getDataByStatus/${status}`,
        {},
        {
          headers: {
            common: {
              "Access-Control-Allow-Origin": "*",
              "Access-Control-Allow-Headers": "Authentication",
              "Content-Type": "application/json",
              Authorization: `Bearer ${localStorage.getItem("token")}`,
              "Referrer-Policy": "no-referrer",
            },
          },
        }
      )
      .then((response) => {
        resolve(response);
      })
      .catch((err) => {
        reject(err);
      });
  });
}
