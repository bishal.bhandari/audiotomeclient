import React, { useEffect, useState } from "react";
import SideNav from "../theme/SideNav";
import {
  GetAudioBookByStatus,
  GetAudiobook,
} from "../../environment/AudiobookService";

const AUDIOBOOKDATA = [
  {
    aid: 1,
    title: "Demo",
    author: "Demo",
    desc: "Hello World.",
    status: "PUBLISHED",
    gender: "Male",
  },
  {
    aid: 2,
    title: "Demo",
    author: "Demo",
    desc: "Hello World.",
    status: "PUBLISHED",
    gender: "Male",
  },
  {
    aid: 3,
    title: "Demo",
    author: "Demo",
    desc: "Hello World.",
    status: "PUBLISHED",
    gender: "Female",
  },
  {
    aid: 4,
    title: "Demo",
    author: "Demo",
    desc: "Hello World.",
    status: "DRAFT",
    gender: "Female",
  },
  {
    aid: 5,
    title: "Demo",
    author: "Demo",
    desc: "Hello World.",
    status: "PUBLISHED",
    gender: "Male",
  },
  {
    aid: 6,
    title: "Demo",
    author: "Demo",
    desc: "Hello World.",
    status: "DRAFT",
    gender: "Female",
  },
];

function AdminAudioBook() {
  const [checkStatus, setCheckStatus] = useState("");
  const [search, setSearch] = useState("");
  const [respData, setRespData] = useState([]);

  const DraftBadge = () => {
    return <div className="badge fs-6 badge-warning">DRAFT</div>;
  };

  const PublishedBadge = () => {
    return <div className="badge fs-6 badge-success">PUBLISHED</div>;
  };

  const ClickedStatusData = (e) => {
    setCheckStatus(e.target.value);
    // console.log(checkStatus);
  };

  const GetAllData = async () => {
    const response = await GetAudiobook().then((resp) => {
      return resp.data.response;
    });
    setRespData(response);
  };

  const GetStatusData = async () => {
    const response = await GetAudioBookByStatus(checkStatus).then((resp) => {
      return resp.data.response;
    });
    setRespData(response);
  };

  useEffect(() => {
    GetAllData();
  }, []);

  return (
    <>
      <SideNav />
      <main className="container mt-5">
        <div className="row">
          <div className="col-sm-6">
            <input
              type="text"
              className="form-control"
              placeholder="Search Here..."
              onChange={(text) => setSearch(text.target.value)}
            />
          </div>
          <div className="col-sm-6">
            <div className="d-flex justify-content-end">
              <button
                type="button"
                className="btn btn-primary"
                onClick={() => {
                  window.location.assign("/admin/audiobook/add-details");
                }}
              >
                Add Audiobook
              </button>
            </div>
          </div>
        </div>
        <div className="d-flex flex-wrap justify-content-center mt-4">
          {AUDIOBOOKDATA
            .filter((item) => {
              return search.toLowerCase() === ""
                ? item
                : item.title.toLowerCase().includes(search);
            })
            .map((i, index) => {
              return (
                <div
                  className="card me-5 my-3"
                  style={{ width: "16rem" }}
                  key={index}
                  loading="lazy"
                >
                  <img
                    src={i.abookImage}
                    className="card-img-top"
                    alt="Fissure in Sandstone"
                  />
                  <div className="card-body">
                    <h5 className="card-title">{i.abookName}</h5>
                    <h6>By {i.abookAuthor}</h6>
                    <p className="text-muted">{i.adescription}</p>
                  </div>
                  <div className="card-footer d-flex justify-content-around">
                    {i.status === "DRAFT" ? <DraftBadge /> : <PublishedBadge />}
                    <button className="btn btn-primary" onClick={() => {window.location.assign(`/admin/view_audiobook_detail/${i.aid}`)}}>
                      <i className="fa-solid fa-eye"></i>
                    </button>
                  </div>
                </div>
              );
            })}
        </div>
      </main>
    </>
  );
}

export default AdminAudioBook;
