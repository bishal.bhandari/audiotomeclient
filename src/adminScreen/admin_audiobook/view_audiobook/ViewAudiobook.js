import React, { useEffect, useState } from "react";
import SideNav from "../../theme/SideNav";
import { useParams } from "react-router-dom";
import {
  DeleteUserById,
  UpdateUserProfileDataById,
  getUserById,
} from "../../../environment/UserService";

export default function ViewAudiobook() {
  const [userData, setUserData] = useState({});
  const [fullName, setFullName] = useState("");
  const [userDataFullname, setUserDataFullname] = useState("");
  const [email, setEmail] = useState("");
  const [dob, setDob] = useState("");
  const [contact, setContact] = useState("");
  const [address, setAddress] = useState("");
  const [profileImage, setProfileImage] = useState({
    fileName: undefined,
    files: undefined,
  });

  const [showPopconfirm, setShowPopconfirm] = useState(false);

  // let fData = new FormData();
  // const file = new FileReader();

  const paramId = useParams("id");

  const GetUserData = async () => {
    const response = await getUserById(paramId.id);
    let data = response.data;
    console.log("data: ", data);
    setUserData(data);
    const FullName =
      data.firstName + " " + data.middleName + " " + data.lastName;
    setUserDataFullname(FullName);
  };

  const Submit = async () => {
    const savingFile = profileImage.files;
    console.log("photo: ", savingFile);
    const fname = fullName.split(" ");
    const userData = {
      firstName: fname[0],
      middleName: fname.length === 3 ? fname[1] : "",
      lastName: fname.length === 3 ? fname[2] : fname[1],
      email: email,
      dob: dob,
      contactNum: contact,
      address: address,
      uprofile: profileImage.files,
    };
    await UpdateUserProfileDataById(paramId.id, JSON.stringify(userData));
  };

  const handleConfirm = (id) => {
    // Implement your confirmation action here
    // DeleteUserById(id)
    console.log("id: ", id);
    // if (showPopconfirm === true) window.location.assign("/admin/users");
  };

  useEffect(() => {
    GetUserData();
    const timer = setInterval(() => GetUserData(), 15000);
    return () => clearInterval(timer);
  }, []);

  return (
    <>
      <SideNav />
      <div className="container mt-5">
        <div className="d-flex justify-content-end">
          <button
            className="btn btn-primary"
            type="button"
            data-mdb-toggle="offcanvas"
            data-mdb-target="#offcanvasExample"
            aria-controls="offcanvasExample"
          >
            <i className="fa-solid fa-pen-to-square"></i> Edit
          </button>
          <div
            className="offcanvas offcanvas-end"
            tabindex="-1"
            id="offcanvasExample"
            aria-labelledby="offcanvasExampleLabel"
          >
            <div className="offcanvas-header">
              <h5 className="offcanvas-title" id="offcanvasExampleLabel">
                Edit User
              </h5>
              <button
                type="button"
                className="btn-close text-reset"
                data-mdb-dismiss="offcanvas"
                aria-label="Close"
              ></button>
            </div>
            <div className="offcanvas-body">
              <div className="form-group mb-3">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Enter your fullname"
                  value={userDataFullname}
                  onChange={(text) => {
                    setFullName(text.target.value);
                  }}
                />
              </div>
              <div className="form-group mb-3">
                <input
                  type="email"
                  className="form-control"
                  placeholder="Enter your email"
                  value={userData.email}
                  onChange={(text) => setEmail(text.target.value)}
                />
              </div>
              <div className="form-group mb-3">
                <input
                  type="date"
                  className="form-control"
                  value={userData.dob}
                  onChange={(text) => setDob(text.target.value)}
                />
              </div>
              <div className="form-group mb-3">
                <input
                  type="number"
                  className="form-control"
                  placeholder="Enter your contact number"
                  onChange={(text) => setContact(text.target.value)}
                />
              </div>
              <div className="form-group mb-3">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Enter your Address"
                  value={userData.address}
                  onChange={(text) => setAddress(text.target.value)}
                />
              </div>
              <div className="form-group mb-3">
                <input
                  type="file"
                  className="form-control"
                  placeholder="Enter your profile image"
                  onChange={(e) => {
                    const file = e.target.files[0];
                    const reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = () => {
                      setProfileImage({
                        filename: e.target.value,
                        files: reader.result,
                      });
                    };
                  }}
                />
              </div>
              <div className="form-group mb-3 d-flex justify-content-center">
                <input
                  type="submit"
                  className="w-50 btn btn-primary"
                  data-mdb-dismiss="offcanvas"
                  onClick={() => Submit()}
                />
              </div>
            </div>
          </div>
          <button
            type="btn"
            className="btn btn-danger mx-3"
            data-mdb-toggle="modal"
            data-mdb-target="#exampleModal"
          >
            <i className="fas fa-trash"></i> Delete
          </button>
          <div
            className="modal fade"
            id="exampleModal"
            tabindex="-1"
            aria-labelledby="exampleModalLabel"
            aria-hidden="true"
          >
            <div className="modal-dialog modal-dialog-centered">
              <div className="modal-content">
                <div className="modal-body">
                  Are you sure you want to delete this user?
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    data-mdb-dismiss="modal"
                  >
                    Close
                  </button>
                  <button
                    type="button"
                    className="btn btn-danger"
                    onClick={() => {
                      handleConfirm(userData.id);
                    }}
                  >
                    Confirm
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="row mt-2">
          <div className="col-sm-4">
            <div className="d-flex justify-content-center">
              <img
                className="rounded-circle"
                alt="Profile pic"
                src={userData.uprofile}
              />
            </div>
          </div>
          <div className="col-sm-8">
            <div className="card">
              <div className="card-body">
                <div className="fs-5">
                  Name: {userData.firstName} {userData.middleName}{" "}
                  {userData.lastName}
                </div>
                <div className="fs-5">Date of Birth (DoB): {userData.dob}</div>
                <div className="fs-5">Role: {userData.role}</div>
              </div>
            </div>
            <div className="card mt-2">
              <div className="card-body">
                <div className="fs-5">Email: {userData.email}</div>
                <div className="fs-5">Contact: {userData.contactNum}</div>
                <div className="fs-5">Address: {userData.address}</div>
              </div>
            </div>
            <div className="card mt-2">
              <div className="card-body">
                <div className="fs-6 fst-italic text-muted">
                  Since: {userData.registerDate}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
