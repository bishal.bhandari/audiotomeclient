import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Home from "./userscreens/home/Home";
import PageNotFound from "./pagenotfound/PageNotFound";
import AudioBooks from "./userscreens/audiobook/AudioBooks";
import Profile from "./userscreens/profile/Profile";
import ChangePassword from "./userscreens/setting/ChangePassword";
import UserLogin from "./auth/userauth/login/UserLogin";
import UserRegister from "./auth/userauth/register/UserRegister";
import Notes from "./userscreens/notes/Notes";
import AboutUs from "./userscreens/aboutus/AboutUs";
import FAQ from "./userscreens/faq/FAQ";
import DashBoard from "./adminScreen/DashBoard/DashBoard";
import AdminAudioBook from "./adminScreen/admin_audiobook/AdminAudioBook";
import AddAudiobook from "./adminScreen/admin_audiobook/add_audiobook/AddAudiobook";
import AdminUser from "./adminScreen/admin_user/AdminUser";
import AddUser from "./adminScreen/admin_user/add_user/AddUser";
import AdminProfile from "./adminScreen/admin_profile/AdminProfile"
import ViewUser from "./adminScreen/admin_user/view_user/ViewUser";
import ViewAudiobook from "./adminScreen/admin_audiobook/view_audiobook/ViewAudiobook";
import ResetPassword from "./auth/resetpassword/ResetPassword";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />
  },
  {
    path: "/audio_books",
    element: <AudioBooks />
  },
  {
    path: "/notes",
    element: <Notes />
  },
  {
    path: "/admin/user",
    element: <AdminUser />
  },
  {
    path: "/admin/userdetails/:id",
    element:<ViewUser />
  },
  {
    path: "/reset-password",
    element: <ResetPassword />
  },
  {
    path: "/admin/view_audiobook_detail/:id",
    element:<ViewAudiobook />
  },
  {
    path: "/admin/profile",
    element: <AdminProfile />
  },
  {
    path: "/admin/adduser",
    element: <AddUser />
  },
  {
    path: "/profile",
    element: <Profile />
  },
  {
    path: "/change-password",
    element: <ChangePassword />
  },
  {
    path: "/login",
    element: <UserLogin />
  },
  {
    path: "/aboutus",
    element: <AboutUs />
  },
  {
    path: "/faq",
    element: <FAQ />
  },
  {
    path: "/register",
    element: <UserRegister />
  },
  {
    path: "*",
    element: <PageNotFound />
  },
  {
    path: "/admin/dashboard",
    element: <DashBoard />
  },
  {
    path: "/admin/audiobook",
    element: <AdminAudioBook />
  },
  {
    path: "/admin/audiobook/add-details",
    element: <AddAudiobook />
  }
]);

function App() {
  return (
    <div className="d-flex flex-column">
      <RouterProvider router={router} />
    </div>
  );
}

export default App;
